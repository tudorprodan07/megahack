import httpServer from 'http';
import koa from 'koa';
import convert from 'koa-convert';
import cors from 'koa-cors';
import bodyparser from 'koa-bodyparser';
import datastore from 'nedb-promise';
import { BankRouter } from './router/BankRouter';
import { UserRouter } from './router/UserRouter';

var app = new koa();
var server = httpServer.createServer(app.callback());

var userStore = datastore({filename: '../users.json', autoload: true});

var bankRouter = new BankRouter(userStore);
var userRouter = new UserRouter(userStore);

app.use(convert(cors()))
   .use(bodyparser())
   .use(bankRouter.routes())
   .use(userRouter.routes());

server.listen(3000);