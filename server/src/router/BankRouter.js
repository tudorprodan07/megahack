import Router from 'koa-router';
import fetch from 'node-fetch';
import { ENDPOINT, AUTH_HEADER_VALUE} from '../config';

const CRYPTO_COINS = ["Bitcoin", "Ethereum", "Ripple", "Dash", "NEO", "Monero", "OmniseGO"];

export class BankRouter extends Router {
    constructor(args) {
        super(args);
        this.userStore = args;
        this
            .get('/', async (ctx) => {
                console.log('HEALTH CHECK');
                ctx.response.status = 200;
                ctx.response.body = 'Pluto app running';
            })
            .get('/banks', async (ctx) => {
                console.log('GET - /banks');
                let resp = await fetch(`${ENDPOINT}/accounts`, {
                    method: 'GET',
                    headers: {
                        "x-auth-token": `${AUTH_HEADER_VALUE}`,
                        "authorization": `${ctx.headers.authorization}`
                    }
                })
                let json = await resp.json();
                ctx.response.status = resp.status;
                ctx.response.body = json._embedded.accounts;
            })
            .post('/payment', async (ctx) => {
                console.log('POST - /payment');
                
                let user = await this.userStore.findOne({token: ctx.headers.authorization});
                if (!user) {
                    await this.userStore.insert({
                        token: ctx.headers.authorization,
                        percent: 10
                    });
                    user = await this.userStore.findOne({token: ctx.headers.authorization});
                }
                let amount = ctx.request.body.amount;
                let percent = user.percent;

                if (!percent) {
                    percent = 10;
                }
                
                let investedAmount = percent / 100 * amount;
                let cryptoCoin = CRYPTO_COINS[Math.floor(Math.random() * CRYPTO_COINS.length)];

                let currentInvestments = [];

                if (user.investments) {
                    currentInvestments = user.investments;
                    let existing = false;
                    
                    currentInvestments.forEach(entry => {
                        if (entry.coin == cryptoCoin) {
                            existing = true;
                            entry.amount = entry.amount + investedAmount;
                        }
                    });
                    
                    if (!existing) {
                        currentInvestments.push({
                            coin: cryptoCoin,
                            amount: investedAmount
                        });
                    }
                } else {
                    currentInvestments.push({
                        coin: cryptoCoin,
                        amount: investedAmount
                    });
                }
                
                user.investments = currentInvestments;
                await this.userStore.update({token: ctx.headers.authorization}, user);

                ctx.response.status = 200;
                ctx.response.body = {};
            });
    }
}