import Router from 'koa-router';
import fetch from 'node-fetch';
import { ENDPOINT, AUTH_HEADER_VALUE } from '../config';

export class UserRouter extends Router {
    constructor(args) {
        super(args);
        this.userStore = args;
        this
            .get('/user', async (ctx) => {
                console.log('GET - /user');
                let user = await this.userStore.findOne({token: ctx.headers.authorization});
                ctx.response.status = 200;
                ctx.response.body = user;
            })
            .post('/user', async (ctx) => {
                console.log('POST - /user');
                await this.userStore.insert({
                    token: ctx.headers.authorization,
                    percent: ctx.request.body.percent
                });
                ctx.response.status = 200;
                ctx.response.body = {};
            });
    }
}