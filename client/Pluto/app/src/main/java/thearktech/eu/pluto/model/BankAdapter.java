package thearktech.eu.pluto.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import thearktech.eu.pluto.R;


/**
 * Created by Lori on 11/4/2017.
 */

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder>{

    private List<Bank> listOfBanks;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_accounts, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Bank dataObjects = listOfBanks.get(position);
        holder.iban.setText(dataObjects.getIban());
        holder.currency.setText(dataObjects.getCurrency());
        holder.imageView.setImageResource(dataObjects.getImage());
    }

    @Override
    public int getItemCount() {
        return listOfBanks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView iban;
        public TextView currency;
        public ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            iban = (TextView)itemView.findViewById(R.id.iban);
            currency = (TextView)itemView.findViewById(R.id.currency);
            imageView = (ImageView)itemView.findViewById(R.id.bank_icon);
        }
    }

    public BankAdapter(List<Bank> listOfBanks){
        this.listOfBanks = listOfBanks;
    }

}
