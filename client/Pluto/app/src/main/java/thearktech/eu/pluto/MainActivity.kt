package thearktech.eu.pluto

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import thearktech.eu.pluto.R.id.tv_bank


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val typeface = ResourcesCompat.getFont(this,R.font.mon)
        tv_bank.setTypeface(typeface)
        tv_crypto.setTypeface(typeface)
        tv_pluto.setTypeface(typeface)
        tv_settings.setTypeface(typeface)

        ll_banks.onClick { startActivity<BankAccountScreen>() }
        ll_cryptocurrency.onClick {startActivity<CryptoCurrencyScreen>()}
        ll_pluto_pay.onClick { startActivity<PlutoPayActivity>() }
        ll_settings.onClick { startActivity<SettingsActivity>() }

        FirebaseMessaging.getInstance().subscribeToTopic("investment_update")
    }
}