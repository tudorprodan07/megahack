package thearktech.eu.pluto

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_settings.*
import thearktech.eu.pluto.utils.Settings

/*
created by Radu
 */
class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        investment_percent.text = Settings.instance.investmentPercent.toString() + " %"
        my_seekbar.progress = Settings.instance.investmentPercent
        my_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                investment_percent.text = p1.toString() + " %"
                Settings.instance.investmentPercent = p1

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
    }

}