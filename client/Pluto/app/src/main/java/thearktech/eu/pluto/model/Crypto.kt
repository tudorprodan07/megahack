package thearktech.eu.pluto.model

/**
 * Created by Tudor on 11/4/2017.
 */
data class Crypto(val coin: String, val amount: Float)