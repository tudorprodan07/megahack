package thearktech.eu.pluto.utils

/**
 * Created by radu on 04.11.2017.
 */

class Settings private constructor() {
    init {
        println("Initialized Settings")
    }

    var investmentPercent: Int = 0

    private object Holder {
        val INSTANCE = Settings()
    }

    companion object {
        val instance: Settings by lazy { Holder.INSTANCE }
    }


}