package thearktech.eu.pluto.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import thearktech.eu.pluto.model.Bank
import thearktech.eu.pluto.model.Payment
import thearktech.eu.pluto.model.User

/**
 * Created by radu on 04.11.2017.
 */

var TOKEN = "Bearer 03fff49d115edd5f8b0248fe01a2c4a09804cbe9"

interface PlutoApiService {


    @GET("/banks")
    fun getAllBanks(@Header("Authorization") token: String): Call<List<Bank>>

    @POST("/payment")
    fun createPayment(@Header("Authorization") token: String, @Body payment: Payment): Call<Void>

    @GET("/user")
    fun getUser(@Header("Authorization") token: String): Call<User>

    companion object {
        fun create(): PlutoApiService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("http://192.168.1.131:3000/")
                    .build()

            return retrofit.create(PlutoApiService::class.java)
        }
    }
}