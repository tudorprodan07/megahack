package thearktech.eu.pluto;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thearktech.eu.pluto.model.Bank;
import thearktech.eu.pluto.model.BankAdapter;
import thearktech.eu.pluto.model.Crypto;
import thearktech.eu.pluto.model.User;
import thearktech.eu.pluto.network.PlutoApiService;
import thearktech.eu.pluto.network.PlutoApiServiceKt;

/**
 * Created by Lori on 11/4/2017.
 */

public class BankAccountScreen extends AppCompatActivity{
    private RecyclerView mRecyclerView;
    private BankAdapter mBankAdapter;
    private List<Bank> mBanks = new ArrayList<>();
    private PlutoApiService plutoApiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        plutoApiService = PlutoApiService.Companion.create();
        setContentView(R.layout.list_of_banks_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mBankAdapter = new BankAdapter(mBanks);
        loadeListOfBanks();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mBankAdapter);

    }
    private void loadeListOfBanks() {

        plutoApiService.getAllBanks(PlutoApiServiceKt.getTOKEN()).enqueue(new Callback<List<Bank>>() {
            @Override
            public void onResponse(Call<List<Bank>> call, Response<List<Bank>> response) {
                for (Bank bank : response.body()) {
                    mBanks.add(bank);
                }

                mBankAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Bank>> call, Throwable t) {
                Log.e("BankAccountScreen", t.getMessage(), t);
            }
        });
    }
}
