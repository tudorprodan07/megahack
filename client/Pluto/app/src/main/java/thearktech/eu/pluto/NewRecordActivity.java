package thearktech.eu.pluto;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Aurelian Cotuna on 11/5/17.
 */

public class NewRecordActivity extends AppCompatActivity {
    private ImageView mRotateImageView;
    private ImageView mEmojiImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_record_layout);

        mRotateImageView = findViewById(R.id.rotation_image);
        mEmojiImageView = findViewById(R.id.icon_image);
        View root = findViewById(R.id.content);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewRecordActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        TextView amountMsg = findViewById(R.id.amount_msg);


        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        Random rnd = new Random();
        amountMsg.setText("New record: " + df.format(2000 + rnd.nextFloat() * rnd.nextInt(5000)) + " EUR");

        rotateView(mRotateImageView);
        scaleInView(mEmojiImageView);

        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.pluto_pay);
        mediaPlayer.start();

    }

    private void rotateView(final View view) {

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    private void scaleInView(final View view) {

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

}


