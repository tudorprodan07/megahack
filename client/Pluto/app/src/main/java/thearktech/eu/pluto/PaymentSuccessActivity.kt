package thearktech.eu.pluto

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity

import org.jetbrains.anko.startActivity

class PaymentSuccessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_success)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity<MainActivity>()
        }, 1500)
    }

}
