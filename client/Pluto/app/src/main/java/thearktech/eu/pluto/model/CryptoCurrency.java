package thearktech.eu.pluto.model;

/**
 * Created by Lori on 11/4/2017.
 */

public class CryptoCurrency {
    public String currencyName;
    public double currencyChange;
    public double currencyValue;
    public int currencyIcon;

    public CryptoCurrency(int currencyIcon, String currencyName, double currencyChange, double currencyValue) {
        this.currencyName = currencyName;
        this.currencyChange = currencyChange;
        this.currencyValue = currencyValue;
        this.currencyIcon = currencyIcon;
    }

    public int getCurrencyIcon() {
        return currencyIcon;
    }

    public void setCurrencyIcon(int currencyIcon) {
        this.currencyIcon = currencyIcon;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public double getCurrencyChange() {
        return currencyChange;
    }

    public void setCurrencyChange(double currencyChange) {
        this.currencyChange = currencyChange;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(double currencyValue) {
        this.currencyValue = currencyValue;
    }
}
