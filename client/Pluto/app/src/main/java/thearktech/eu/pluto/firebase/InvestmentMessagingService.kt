package thearktech.eu.pluto.firebase

import android.app.Notification
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import thearktech.eu.pluto.R
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import thearktech.eu.pluto.MainActivity
import thearktech.eu.pluto.NewRecordActivity


/**
 * Created by Tudor on 11/5/2017.
 */
class InvestmentMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(msg: RemoteMessage?) {
        super.onMessageReceived(msg)
        val contentIntent = PendingIntent.getActivity(this, 0,
                Intent(this, NewRecordActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
        val mBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.abc_ic_star_black_16dp)
                .setContentTitle("Investment update")
                .setContentText("Your investment reached a new high!")
                .setContentIntent(contentIntent)
                .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)

        val mNotifyMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotifyMgr.notify(1, mBuilder.build())
    }
}