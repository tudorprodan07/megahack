package thearktech.eu.pluto.model

/**
 * Created by Tudor on 11/4/2017.
 */
data class Payment(val iban: String, val amount: Float, val currency: String)