package thearktech.eu.pluto.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import thearktech.eu.pluto.R;

/**
 * Created by Lori on 11/4/2017.
 */

public class CryptoCurrencyAdapter extends RecyclerView.Adapter<CryptoCurrencyAdapter.ViewHolder> {
    private List<CryptoCurrency> mListOfCurrencyes;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cryptocurrency_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CryptoCurrency cryptoCurrency = mListOfCurrencyes.get(position);
        holder.currencyIcon.setImageResource(cryptoCurrency.getCurrencyIcon());
        holder.currencyName.setText(cryptoCurrency.getCurrencyName());
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);;
        holder.currencyChange.setText(df.format(cryptoCurrency.getCurrencyChange()) + "%");
        holder.currencyAmount.setText(df.format(cryptoCurrency.getCurrencyValue()) + " $");
    }

    @Override
    public int getItemCount() {
        return mListOfCurrencyes.size();
    }

    public CryptoCurrencyAdapter(List<CryptoCurrency> listOfCurrencyes) {
        this.mListOfCurrencyes = listOfCurrencyes;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView currencyIcon;
        public TextView currencyAmount;
        public TextView currencyName;
        public TextView currencyChange;
        public ViewHolder(View itemView) {
            super(itemView);
            currencyIcon = (ImageView)itemView.findViewById(R.id.currency_icon);
            currencyName = (TextView)itemView.findViewById(R.id.currency_name);
            currencyAmount = (TextView)itemView.findViewById(R.id.currency_amount);
            currencyChange = (TextView)itemView.findViewById(R.id.currency_change);
        }
    }
}
