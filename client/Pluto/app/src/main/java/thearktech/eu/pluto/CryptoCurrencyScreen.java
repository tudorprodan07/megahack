package thearktech.eu.pluto;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thearktech.eu.pluto.model.Crypto;
import thearktech.eu.pluto.model.CryptoCurrency;
import thearktech.eu.pluto.model.CryptoCurrencyAdapter;
import thearktech.eu.pluto.model.User;
import thearktech.eu.pluto.network.PlutoApiService;
import thearktech.eu.pluto.network.PlutoApiServiceKt;

/**
 * Created by Lori on 11/4/2017.
 */

public class CryptoCurrencyScreen extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private List<CryptoCurrency> mListOfCryptoCurrencyes = new ArrayList<>();
    private CryptoCurrencyAdapter mCryptoCurrencyAdapter;
    private PlutoApiService plutoApiService = PlutoApiService.Companion.create();
    private Random random;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crypto_currency_list_layout);
        mRecyclerView = (RecyclerView)findViewById(R.id.crypto_currency_recycler_view);


        mCryptoCurrencyAdapter = new CryptoCurrencyAdapter(mListOfCryptoCurrencyes);
        loadCryptoCurrencyes();
        RecyclerView.LayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mCryptoCurrencyAdapter);
    }

    public void loadCryptoCurrencyes() {
        plutoApiService.getUser(PlutoApiServiceKt.getTOKEN()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User body = response.body();
                if (body == null || body.getInvestments() == null) {
                    return;
                }
                for (Crypto crypto : body.getInvestments()) {
                    random = new Random();
                    CryptoCurrency cryptoCurrency = new CryptoCurrency(getDrawableForCoin(crypto.getCoin()), crypto.getCoin(), random.nextFloat() * random.nextInt(10), crypto.getAmount());
                    mListOfCryptoCurrencyes.add(cryptoCurrency);
                }
                mCryptoCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("CryptoCurrencyScreen", t.getMessage(), t);
            }
        });
    }

    private int getDrawableForCoin(String coin) {
        switch (coin) {
            case "Bitcoin":
                return R.drawable.bitcoin_logo;
            case "Ethereum":
                return R.drawable.etherium_logo;
            case "Ripple":
                return R.drawable.ripple_logo;
            case "Dash":
                return R.drawable.dash_logo;
            case "NEO":
                return R.drawable.neo_logo;
            case "Monero":
                return R.drawable.monero_logo;
            case "OmniseGO":
                return R.drawable.omg_logo;
            default:
                return R.drawable.bitcoin_logo;
        }
    }
}
