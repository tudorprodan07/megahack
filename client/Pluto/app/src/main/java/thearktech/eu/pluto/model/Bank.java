package thearktech.eu.pluto.model;

/**
 * Created by Lori on 11/4/2017.
 */

public class Bank {
    public String iban;
    public String currency;
    public int image;

    public Bank( String iban, String currency, int image) {
        this.iban = iban;
        this.currency = currency;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
