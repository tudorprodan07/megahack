package thearktech.eu.pluto

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log

import kotlinx.android.synthetic.main.activity_pluto_pay.*
import kotlinx.android.synthetic.main.content_pluto_pay.*
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import thearktech.eu.pluto.model.Payment
import thearktech.eu.pluto.network.PlutoApiService
import thearktech.eu.pluto.network.TOKEN

class PlutoPayActivity : AppCompatActivity() {

    val plutoApiService: PlutoApiService = PlutoApiService.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pluto_pay)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        submit_btn.setOnClickListener {
            plutoApiService.createPayment(TOKEN, Payment(iban.text.toString(), amount.text.toString().toFloat(), "RON")).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                    Log.d("PlutoPayActivity", "success")
                    startActivity<PaymentSuccessActivity>()
                }

                override fun onFailure(call: Call<Void>?, t: Throwable?) {
                    Log.e("PlutoPayActivity", t?.message, t)
                    startActivity<PaymentSuccessActivity>()
                }
            })
        }
    }
}
