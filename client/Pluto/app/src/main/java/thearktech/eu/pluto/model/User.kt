package thearktech.eu.pluto.model

/**
 * Created by Tudor on 11/4/2017.
 */
class User(val token: String, val percent: Int, val investments: List<Crypto>?)